import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'lottie',
  webDir: 'www',
  bundledWebRuntime: false,
  plugins: {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    SplashScreen:{
      launchAutoHide: true,
      launchShowDuration: 0,
      backgroundColor: '#ffffff',
    }
  },
  cordova: {
    preferences: {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      LottieFullScreen: 'false',
      // eslint-disable-next-line @typescript-eslint/naming-convention
      LottieHideAfterAnimationEnd: 'true',
      // eslint-disable-next-line @typescript-eslint/naming-convention
      // LottieHideTimeout: '5000',
      // eslint-disable-next-line @typescript-eslint/naming-convention
      LottieAnimationLocation: 'public/assets/splash.json'
    }
  }
};

export default config;
