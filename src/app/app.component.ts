import { Component } from '@angular/core';
import { LottieSplashScreen } from '@awesome-cordova-plugins/lottie-splash-screen/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  
  constructor(private lottieSplashScreen: LottieSplashScreen) {
    this.splash();
  }

  splash() {
    this.lottieSplashScreen.hide();
    this.lottieSplashScreen.show('public/assets/splash.json', false);
  }
}
